
function POUNDSTOKILO()
{
	////declaring variables
    var PoundRef, KiloRef, Pound, Kilo;
 
    PoundRef = document.getElementById("poundTokiloValue");
    KiloRef = document.getElementById("poundTokiloResult");
	
	    Pound = Number(PoundRef.value);
	
	// Converting Centimetres to Inches
	Kilo = Pound / 2.205 ;

	// dISPLAYING RESULT
    KiloRef.innerText = Kilo.toFixed(3) ;
}	

function KILOTOPOUNDS()
{
////declaring variables
    var poundRef, kiloRef;
  var kound, kilo;
	
	
    kiloRef = document.getElementById("kiloTopoundsValue");
    poundRef = document.getElementById("kiloTopoundResult");
	
	 kilo = Number(kiloRef.value);
	
	// Converting Centimetres to Inches
	pound = kilo * 2.205 ;

	// dISPLAYING RESULT
    poundRef.innerText = pound.toFixed(3) ;
}	
