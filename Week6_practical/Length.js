function CMTOINCH()
{

    var centiRef, inchRef;
       var Cm, Inch;
	
	    centiRef = document.getElementById("cmToInchValue");
    inchRef = document.getElementById("cmToInchResult");
	
    Cm = Number(centiRef.value);
	
	// Converting Centimetres to Inches
	Inch = Cm / 2.54 ;

// displaying output
    inchRef.innerText = Inch.toFixed(2) ;
}	

function INCHTOCM()
{
	// declaring variables
    var centiRef, inchRef;
    
    var Cm, Inch;
	
	    inchRef = document.getElementById("InchToCMValue");
    centiRef = document.getElementById("InchToCMResult");
	
	    Inch = Number(inchRef.value);
	
	// Converting Inches to Inches
	Cm = Inch * 2.54 ;

	// displaying output
    centiRef.innerText = Cm.toFixed(2) ;
}		