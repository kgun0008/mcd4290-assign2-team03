let routesDB = JSON.parse(localStorage.getItem('RouteList'));
let routeList = routesDB.routeList ? routesDB.routeList : []
<<<<<<< HEAD
console.log(JSON.stringify(routesDB))
MapRoute(routeList)

function MapRoute(routes){

    //First filter out ships that are already en-route, just render that havent gone
    routes.forEach(route => {

    let sel = document.getElementById('route-list')
    var option = document.createElement("option");
    option.text = `  ${route.sourcePort}  to  ${route.destinationPort}`
    sel.add(option)

  });

}
=======
// console.log(JSON.stringify(routesDB))
// MapRoute(routeList)

// function MapRoute(routes){

//     //First filter out ships that are already en-route, just render that havent gone
//     let RouteRow = document.createElement("ul");
//     routes.forEach(route => {

//     // let sel = document.getElementById('route-list')
//     // var option = document.createElement("option");
//     // option.text = `  ${route.sourcePort}  to  ${route.destinationPort}`
//     // sel.add(option)
//     let z = document.createElement('li')
//     z.className = "route-item"
//     let a = document.createElement('a')
//     a.onclick = function() {saveChosenRoute(route.sourcePort,route.destinationPort);}
//     // a.href = "viewRoute.html"
//     a.innerHTML = `${route.sourcePort} to ${route.destinationPort}`
//     // let routeRowContents = `
//     //   <li>${route.sourcePort} to ${route.destinationPort}</li>
//     // `;
//     z.appendChild(a)
//     RouteRow.appendChild(z)
//     let parent = document.getElementById('route-list')
//     parent.appendChild(RouteRow)

//   });

// }


function routeDetails(){
  const routeName = JSON.parse(localStorage.getItem('SelectedRoute'))

  const SelectedRoute = routeList.filter((route)=>{
    return route.sourcePort == routeName.sourcePort
  })
  console.log(SelectedRoute)
  const sourceDiv = document.getElementById('source')
  const destinationDiv = document.getElementById('destination')
  const shipDiv = document.getElementById('ship')
  const dateDiv = document.getElementById('date')
  const nameDiv = document.getElementById('name')
  const costDiv = document.getElementById('cost')
  const timeDiv = document.getElementById('time')
  costDiv.innerText = SelectedRoute[0].cost
  nameDiv.innerText = SelectedRoute[0].name
  timeDiv.innerText = SelectedRoute[0].time
  sourceDiv.innerText = SelectedRoute[0].sourcePort
  destinationDiv.innerText = SelectedRoute[0].destinationPort
  shipDiv.innerText = SelectedRoute[0].ship
  dateDiv.innerText = SelectedRoute[0].startDate

}

function DeleteRoute(routename){
  const targetRoute = JSON.parse(localStorage.getItem('SelectedRoute'))
  const removedRoute = routeList.filter((route)=>{
    return  targetRoute.sourcePort != route.sourcePort
  })

  let allRouteDB = {
    routeList : removedRoute
  };
  var jsonDeck = JSON.stringify(allRouteDB);

  localStorage.setItem('RouteList', jsonDeck);

  setTimeout(()=>{
    window.location.replace("index.html")
  }, 800)
}


function updateMapCordinates(){
  let routeDB = JSON.parse(localStorage.getItem('RouteList'));
  let routesList = routeDB.routeList ? routeDB.routeList : []

  const routeName = JSON.parse(localStorage.getItem('SelectedRoute'))

  const SelectedRoute = routesList.filter((route)=>{
    return routeName.sourcePort == route.sourcePort
  })
  let portsDB = JSON.parse(localStorage.getItem('PortList'));
  let portList = portsDB ? portsDB : [];
  const intrestSourcePorts = portList.filter((port)=>{
    return SelectedRoute[0].sourcePort == port.name 
  })
  const intrestDestinationPorts = portList.filter((port)=>{
    return SelectedRoute[0].destinationPort == port.name 
  })
  console.log(`por her ${JSON.stringify(intrestDestinationPorts)}`)
  let sourceCords = [intrestSourcePorts[0].lng, intrestSourcePorts[0].lat]
  let destCords = [intrestDestinationPorts[0].lng, intrestDestinationPorts[0].lat]
  mapSourceTodest(sourceCords,destCords)
}

updateMapCordinates()














function mapSourceTodest(sourcePort, destPort){
 
  mapboxgl.accessToken = 'pk.eyJ1IjoibXVzYWI5ODciLCJhIjoiY2tlMmNvNjgzMDgwYTJzb2I1MmdlMTVkcSJ9.R5WwJb-qcA75bHn7L3gQsA';
  var map = new mapboxgl.Map({
  container: 'map',
  style: 'mapbox://styles/mapbox/streets-v11',
  center: sourcePort,
  zoom: 2
  });


          var marker = new mapboxgl.Marker({color: '#0ba11f'})
            .setLngLat(sourcePort)
            .addTo(map);
        var marker2 = new mapboxgl.Marker({color: '#db0b2a'})
            .setLngLat(destPort)
            .addTo(map);
  
  var distanceContainer = document.getElementById('distance');
  // GeoJSON object to hold our measurement features
  var geojson_markers = {
  'type': 'FeatureCollection',
  'features': [{
      type: 'Feature',
      geometry: {
          type: 'Point',
          coordinates: sourcePort
          },
          properties: {
              'id': "start"
          }
      },
      {
          type: 'Feature',
          geometry: {
          type: 'Point',
          coordinates: destPort
          },
          properties: {
              'id': "end"
          }
      }]
  };
  // add initial markers to map
  function addMarkers() {
      geojson_markers.features.forEach(function(marker) {

      // create a HTML element for each feature
      var el = document.createElement('div');
      if(marker.properties.id.trim().localeCompare("end")) {
          el.setAttribute("class", "end-marker");
      } else {
        el.setAttribute("class", "start-marker");
      }
      el.className = 'marker';

      // make a marker for each feature and add to the map
      new mapboxgl.Marker(el)
      .setLngLat(marker.geometry.coordinates)
      .setPopup(new mapboxgl.Popup({ offset: 25 }) // add popups
          .setHTML('<h3>' + marker.properties.title + '</h3><p>' + marker.properties.description + '</p>'))
      .addTo(map);
      });
  }
  // adding markers for starting and ending point
  addMarkers()
  // 
  geojson = {
        type: 'FeatureCollection',
        features: [{
        type: 'Feature',
        geometry: {
            type: 'Point',
            coordinates: sourcePort
            },
        properties: {
            title: 'Mapbox',
            description: 'Washington, D.C.',
            id:"start"
            }
        },
        {
        type: 'Feature',
        geometry: {
        type: 'Point',
        coordinates: destPort
            },
        properties: {
        title: 'Mapbox',
        description: 'San Francisco, California',
        id:"end"
            }
        },
        {
        type: 'Feature',
        geometry: {
        type: 'LineString',
        coordinates: [sourcePort,destPort]
            }
        }
    
        ]
    };
     
    // Used to draw a line between points
    var linestring = {
    'type': 'Feature',
    'geometry': {
    'type': 'LineString',
    'coordinates': []
    }
    };
     
    map.on('load', function() {
    map.addSource('geojson', {
    'type': 'geojson',
    'data': geojson
    });
     
    // Add styles to the map
    map.addLayer({
    id: 'measure-points',
    type: 'circle',
    source: 'geojson',
    paint: {
    'circle-radius': 5,
    'circle-color': 'orange'
    },
    filter: ['in', '$type', 'Point']
    });
    map.addLayer({
    id: 'measure-lines',
    type: 'line',
    source: 'geojson',
    layout: {
    'line-cap': 'round',
    'line-join': 'round'
    },
    paint: {
    'line-color': '#000',
    'line-width': 2.5
    },
    filter: ['in', '$type', 'LineString']
    });
     
    map.on('click', function(e) {
    var features = map.queryRenderedFeatures(e.point, {
    layers: ['measure-points']
    });
     
    // Remove the linestring from the group
    // So we can redraw it based on the points collection
    if (geojson.features.length > 1) geojson.features.pop();
    // Clear the Distance container to populate it with a new value
    distanceContainer.innerHTML = '';
     
    // If a feature was clicked, remove it from the map
    if (features.length) {
    var id = features[0].properties.id;
    // Not allow first and last point to be deleted
    if( id != "start" && id != "end"){
        geojson.features = geojson.features.filter(function(point) {
            return point.properties.id !== id;
            })
     }
    } else {
        var point = {
            'type': 'Feature',
            'geometry': {
            'type': 'Point',
            'coordinates': [e.lngLat.lng, e.lngLat.lat]
            },
            'properties': {
            'id': String(new Date().getTime())
        }
        };
        //checking distance between two points
        //at this point we have removed the linestring(last value from geojson)
        var from = turf.point(geojson.features[geojson.features.length-2].geometry.coordinates);
        var to = turf.point(point.geometry.coordinates);
        var options = {units:'kilometers'};
        var distanceBetween = turf.distance(from, to, options);
        if(distanceBetween > 100){
            geojson.features.splice(geojson.features.length-1,0,point);
        } else {
            var value = document.createElement('div');
            value.textContent =
            'Distance should be greater than 100km';
            distanceContainer.appendChild(value);
        }
    }
     // mapping linestring with points 
    if (geojson.features.length > 1) {
        linestring.geometry.coordinates = geojson.features.map(function(
        point
        ) {
        return point.geometry.coordinates;
        });
        
        geojson.features.push(linestring);
        
        // Populate the distanceContainer with total distance
        var value = document.createElement('pre');
        value.textContent =
        'Distance: ' +
        turf.length(linestring).toLocaleString() +
        'km';
        distanceContainer.appendChild(value);
    }
    distance = turf.length(linestring)
    map.getSource('geojson').setData(geojson);
    });
    });
     
  
  map.on('mousemove', function(e) {
      // display co-ordinate values
      document.getElementById('info').innerHTML =
                  // e.point is the x, y coordinates of the mousemove event relative
                  // to the top-left corner of the map
                  JSON.stringify(e.point) +
                  '<br />' +
                  // e.lngLat is the longitude, latitude geographical position of the event
                  JSON.stringify(e.lngLat.lng);

      
      var features = map.queryRenderedFeatures(e.point, {
      layers: ['measure-points']
      });
      // UI indicator for clicking/hovering a point on the map
      map.getCanvas().style.cursor = features.length
      ? 'pointer'
      : 'crosshair';
  });
  // console.log("Ended")

  
}











//Intended to run initially
let shipDB = JSON.parse(localStorage.getItem('ShipList'));
let shipList = shipDB.shipList ? shipDB.shipList : []
async function getShips (){
  const data = await  fetch('https://eng1003.monash/api/v1/ships/')
  .then(response => response.json())
  .then(data =>{
    return data
  });
  return data
}
getShips()
.then(ship =>{
  let allships = [...ship.ships, ...shipList]
  var jsonDeck = JSON.stringify(allships);
  localStorage.setItem('ShipList', jsonDeck);
})

//Save ports
let portDB = JSON.parse(localStorage.getItem('PortList'));
let portList = portDB.portList ? portDB.portList : []
async function AddPorts (){
  const data = await  fetch('https://eng1003.monash/api/v1/ports/')
  .then(response => response.json())
  .then(data =>{
    return data
  });
  return data
}
AddPorts()
.then(port =>{
  let allPorts = [...port.ports, ...portList]
  var jsonDeck = JSON.stringify(allPorts);
  localStorage.setItem('PortList', jsonDeck);
})
>>>>>>> master

