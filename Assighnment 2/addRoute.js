let shipDB = JSON.parse(localStorage.getItem('ShipList'));
let shipList = shipDB ? shipDB : []
// console.log(shipDB)
MapShip(shipList)

//Get ships from API then append to data from local storage
// async function getShips (){
//     const data = await  fetch('https://eng1003.monash/api/v1/ships/')
//     .then(response => response.json())
//     .then(data =>{
//        return data
//     });
//     return data
//   }
//   getShips()
//   .then(ship =>{
//     let allships
//     shipList ? allships = [...ship.ships, ...shipList] : allships = [...ship.ships]
//     // let allships = [...ship.ships, ...shipList]
//     MapShip(allships)
//     shipList = allships
//   })


//Get list of ships and append to the DOM
function MapShip(ships){
    

    ships.filter(ship => ship.status == 'available').forEach(ship => {

    let sel = document.getElementById('ship-option')
    var option = document.createElement("option");
    option.setAttribute('id','option-ship')
    option.setAttribute('value',`${ship.name}`)
    option.text = ship.name
    sel.add(option)
    console.log(ship)

  });

}
// async function getShips (){
//     const data = await  fetch('https://eng1003.monash/api/v1/ships/')
//     .then(response => response.json())
//     .then(data =>{
//        return data
//     });
//     return data
//   }
//   getShips()
//   .then(ship =>{
//     let allships = [...ship.ships, ...shipList]
//     console.log(allships)
//     allships.filter(ship => ship.status == 'available').forEach(ship => {
//         console.log(ship.name)
//         let sel = document.getElementById('ship-option')
//         var option = document.createElement("option");
//         option.text = ship.name
//         sel.add(option)
    
//       });
//   })

    //Get list of ports and append to the DOM
let portDB = JSON.parse(localStorage.getItem('PortList'));
let sourcePortList = portDB ? portDB : []
// console.log(` sourgdakhhhh ${portDB}`)

MapSourcePort(sourcePortList)
MapDestinationPort(sourcePortList)
// getPorts()
// async function getPorts (){
//     const data = await  fetch('https://eng1003.monash/api/v1/ports/')
//     .then(response => response.json())
//     .then(data =>{
//        return data
//     });
//     return data
//   }
//   getPorts()
//   .then(port =>{
//     let allPorts = [...port.ports, ...sourcePortList]
//     MapSourcePort(allPorts)
//     MapDestinationPort(allPorts)
//   })


function MapSourcePort(ports){

    ports.forEach(port => {
        let select = document.getElementById('source-port')
        var option = document.createElement("option");
        option.text = port.name
        select.add(option)
    })
}


function MapDestinationPort(ports){
    ports.forEach(port => {
        let select = document.getElementById('destination-port')
        var option = document.createElement("option");
        option.text = port.name
        select.add(option)
    })
}


let routeDB = JSON.parse(localStorage.getItem('RouteList'));
let routeList = routeDB ? routeDB.routeList : []
console.log(routeDB)

function addRoute() {
    let route = createRoute();

    updateRouteList(route);
    window.location.replace("index.html");
}

function createRoute() {
    const dista = document.getElementById('distance').innerText
    const shipName = document.getElementById('ship-option').value
    const selectedShip = shipList.filter((ship)=>{
        return ship.name == shipName
    })
    const parsedDist = dista.replace('km','').replace(',','').replace(' ','').replace('Distance:','')

    const time = parseInt(parsedDist) / selectedShip[0].maxSpeed
    
    const travelCost = parseInt(parsedDist) * selectedShip[0].cost
    const name = document.getElementById('name-option').value
    let routeSpecs = {
        ship : document.getElementById('ship-option').value,
        startDate : document.getElementById('date-option').value,
        sourcePort : document.getElementById('source-port').value,
        destinationPort : document.getElementById('destination-port').value,
        distance: parseInt(parsedDist),
        time: time,
        cost : travelCost,
        name: name
        // status : document.getElementById('Status').value
    }

    return new Route(routeSpecs);
}
function updateRouteList(route) {
    routeList.push(route);
    console.log(`rut 45 ${JSON.stringify(routeList)}`)
    let allRouteDB = {
        routeList : routeList
    };

    if (typeof (Storage) !== "undefined") {
        // TODO: Stringify deckInstance to a JSON string
        //var jsonDeck = JSON.stringify(ship);
        var jsonDeck = JSON.stringify(allRouteDB);

        // TODO: store this JSON string to local storage
        //       using the key STORAGE_KEY.
        //localStorage.setItem('ShipList', jsonDeck);
        localStorage.setItem('RouteList', jsonDeck);

        console.log(localStorage.getItem('RouteList'));
    } else {
        console.log("Error: localStorage is not supported by current browser.");
    }
}



// This function Queries Mapbox with Updated co-rdinates of dest and source
function updateMap(){
    const source = document.getElementById('source-port').value
    const destination = document.getElementById('destination-port').value
    //We filter out the ports that were selected by user
    let destinationPort = sourcePortList.filter(port =>{
        return port.name == destination
    })  
    let sourcePort = sourcePortList.filter(port =>{
        return port.name == source
    })  
    //Here we input dynamic co-ordinates to the map by calling below function
    mapSourceTodest([destinationPort[0].lng, destinationPort[0].lat],[ sourcePort[0].lng,sourcePort[0].lat])

}
function updateTimeCost(){

    const dista = document.getElementById('distance').innerText
    const shipName = document.getElementById('ship-option').value

    const selectedShip = shipList.filter((ship)=>{

        return shipName == ship.name
    })

    const parsedDist = dista.replace('km','').replace(',','').replace(' ','').replace('Distance:','')
    const time = parseInt(parsedDist) / selectedShip[0].maxSpeed

    const travelCost = parseInt(parsedDist,10) * selectedShip[0].cost

    const timeDiv = document.getElementById('time')
    timeDiv.innerText = parseInt(time)
    const costDiv = document.getElementById('cost')
    costDiv.innerText = parseInt(travelCost)
}

//We need to call this function for initial map to render
mapSourceTodest([14.8283, 38.58223],[ 37.43593,24.9411])

function mapSourceTodest(sourcePort, destPort){
 
  mapboxgl.accessToken = 'pk.eyJ1IjoibXVzYWI5ODciLCJhIjoiY2tlMmNvNjgzMDgwYTJzb2I1MmdlMTVkcSJ9.R5WwJb-qcA75bHn7L3gQsA';
  var map = new mapboxgl.Map({
  container: 'map',
  style: 'mapbox://styles/mapbox/streets-v11',
  center: sourcePort,
  zoom: 4
  });


          var marker = new mapboxgl.Marker({color: '#0ba11f'})
            .setLngLat(sourcePort)
            .addTo(map);
        var marker2 = new mapboxgl.Marker({color: '#db0b2a'})
            .setLngLat(destPort)
            .addTo(map);
  
  var distanceContainer = document.getElementById('distance');
  // GeoJSON object to hold our measurement features
  var geojson_markers = {
  'type': 'FeatureCollection',
  'features': [{
      type: 'Feature',
      geometry: {
          type: 'Point',
          coordinates: sourcePort
          },
          properties: {
              'id': "start"
          }
      },
      {
          type: 'Feature',
          geometry: {
          type: 'Point',
          coordinates: destPort
          },
          properties: {
              'id': "end"
          }
      }]
  };
  // add initial markers to map
  function addMarkers() {
      geojson_markers.features.forEach(function(marker) {

      // create a HTML element for each feature
      var el = document.createElement('div');
      if(marker.properties.id.trim().localeCompare("end")) {
          el.setAttribute("class", "end-marker");
      } else {
        el.setAttribute("class", "start-marker");
      }
      el.className = 'marker';

      // make a marker for each feature and add to the map
      new mapboxgl.Marker(el)
      .setLngLat(marker.geometry.coordinates)
      .setPopup(new mapboxgl.Popup({ offset: 25 }) // add popups
          .setHTML('<h3>' + marker.properties.title + '</h3><p>' + marker.properties.description + '</p>'))
      .addTo(map);
      });
  }
  // adding markers for starting and ending point
  addMarkers()
  // 
  geojson = {
        type: 'FeatureCollection',
        features: [{
        type: 'Feature',
        geometry: {
            type: 'Point',
            coordinates: sourcePort
            },
        properties: {
            title: 'Mapbox',
            description: 'Washington, D.C.',
            id:"start"
            }
        },
        {
        type: 'Feature',
        geometry: {
        type: 'Point',
        coordinates: destPort
            },
        properties: {
        title: 'Mapbox',
        description: 'San Francisco, California',
        id:"end"
            }
        },
        {
        type: 'Feature',
        geometry: {
        type: 'LineString',
        coordinates: [sourcePort,destPort]
            }
        }
    
        ]
    };
     
    // Used to draw a line between points
    var linestring = {
    'type': 'Feature',
    'geometry': {
    'type': 'LineString',
    'coordinates': []
    }
    };
     
    map.on('load', function() {
    map.addSource('geojson', {
    'type': 'geojson',
    'data': geojson
    });
     
    // Add styles to the map
    map.addLayer({
    id: 'measure-points',
    type: 'circle',
    source: 'geojson',
    paint: {
    'circle-radius': 5,
    'circle-color': 'orange'
    },
    filter: ['in', '$type', 'Point']
    });
    map.addLayer({
    id: 'measure-lines',
    type: 'line',
    source: 'geojson',
    layout: {
    'line-cap': 'round',
    'line-join': 'round'
    },
    paint: {
    'line-color': '#000',
    'line-width': 2.5
    },
    filter: ['in', '$type', 'LineString']
    });
     
    map.on('click', function(e) {
    var features = map.queryRenderedFeatures(e.point, {
    layers: ['measure-points']
    });
     
    // Remove the linestring from the group
    // So we can redraw it based on the points collection
    if (geojson.features.length > 1) geojson.features.pop();
    // Clear the Distance container to populate it with a new value
    distanceContainer.innerHTML = '';
     
    // If a feature was clicked, remove it from the map
    if (features.length) {
    var id = features[0].properties.id;
    // Not allow first and last point to be deleted
    if( id != "start" && id != "end"){
        geojson.features = geojson.features.filter(function(point) {
            return point.properties.id !== id;
            })
     }
    } else {
        var point = {
            'type': 'Feature',
            'geometry': {
            'type': 'Point',
            'coordinates': [e.lngLat.lng, e.lngLat.lat]
            },
            'properties': {
            'id': String(new Date().getTime())
        }
        };
        //checking distance between two points
        //at this point we have removed the linestring(last value from geojson)
        var from = turf.point(geojson.features[geojson.features.length-2].geometry.coordinates);
        var to = turf.point(point.geometry.coordinates);
        var options = {units:'kilometers'};
        var distanceBetween = turf.distance(from, to, options);
        if(distanceBetween > 100){
            geojson.features.splice(geojson.features.length-1,0,point);
        } else {
            var value = document.createElement('div');
            value.textContent =
            'Distance should be greater than 100km';
            distanceContainer.appendChild(value);
        }
    }
     // mapping linestring with points 
    if (geojson.features.length > 1) {
        linestring.geometry.coordinates = geojson.features.map(function(
        point
        ) {
        return point.geometry.coordinates;
        });
        
        geojson.features.push(linestring);
        
        // Populate the distanceContainer with total distance
        var value = document.createElement('pre');
        value.textContent =
        'Distance: ' +
        turf.length(linestring).toLocaleString() +
        'km';
        distanceContainer.appendChild(value);
    }
    distance = turf.length(linestring)
    map.getSource('geojson').setData(geojson);
    });
    });
     
  
  map.on('mousemove', function(e) {
      // display co-ordinate values
      document.getElementById('info').innerHTML =
                  // e.point is the x, y coordinates of the mousemove event relative
                  // to the top-left corner of the map
                  JSON.stringify(e.point) +
                  '<br />' +
                  // e.lngLat is the longitude, latitude geographical position of the event
                  JSON.stringify(e.lngLat.lng);

      
      var features = map.queryRenderedFeatures(e.point, {
      layers: ['measure-points']
      });
      // UI indicator for clicking/hovering a point on the map
      map.getCanvas().style.cursor = features.length
      ? 'pointer'
      : 'crosshair';
  });
  // console.log("Ended")

  
}