

////////////////////////viewing ships
let shipDB = JSON.parse(localStorage.getItem('ShipList'));
let shipList = shipDB ? shipDB : [];

  //fetch the API for ship list then append to local list
async function getShips (){
  const data = await  fetch('https://eng1003.monash/api/v1/ships/')
  .then(response => response.json())
  .then(data =>{
     return data
  });
  return data
}
getShips()
.then(ship =>{
  let allships
  shipList ? allships = [...shipList] : allships = [...ship.ships]
  // let allships = [...ship.ships, ...shipList]
  allships.forEach(ship => {
    var shipTR = createShipTR(ship);
    insertShipTR(shipTR);
  });
})


///shiptable
  function createShipTR(ship){
    var {name, range, maxSpeed, desc, cost, status} = ship;
    let shipRow = document.createElement("tr");
    let shipRowContents = `
      <td>${name}</td>
      <td>${range}</td>
      <td>${maxSpeed}</td>
      <td>${desc}</td>
      <td>${cost}</td>
      <td>${status}</td>
    `;
    shipRow.innerHTML = shipRowContents;
      shipRow

    return shipRow;
  }

  function insertShipTR(shipRow){
    document
      .querySelector("#shipTableData")
      .appendChild(shipRow);
  }
  