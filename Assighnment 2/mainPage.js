let routesDB = JSON.parse(localStorage.getItem('RouteList'));
let routeList = routesDB ? routesDB.routeList: []
console.log(routeList)
MapRoute(routeList)

function MapRoute(routes){

    //First filter out ships that are already en-route, just render that havent gone
    let RouteRow = document.createElement("ul");
    routes.forEach(route => {

    // let sel = document.getElementById('route-list')
    // var option = document.createElement("option");
    // option.text = `  ${route.sourcePort}  to  ${route.destinationPort}`
    // sel.add(option)
    let z = document.createElement('li')
    z.className = "route-item"
    let a = document.createElement('a')
    a.onclick = function() {saveChosenRoute(route.sourcePort,route.destinationPort);}
    // a.href = "viewRoute.html"
    a.innerHTML = `${route.sourcePort} to ${route.destinationPort}`
    // let routeRowContents = `
    //   <li>${route.sourcePort} to ${route.destinationPort}</li>
    // `;
    z.appendChild(a)
    RouteRow.appendChild(z)
    let parent = document.getElementById('route-list')
    parent.appendChild(RouteRow)

  });

}

function saveChosenRoute(source, destination){
    const routeTitle = {
      sourcePort: source,
      destinationPort: destination
    }
    const ready_to_save = JSON.stringify(routeTitle)
    localStorage.setItem('SelectedRoute', ready_to_save);
  
    // console.log(localStorage.getItem('SelectedRoute'));
  
    setTimeout(()=>{
      window.location.replace("viewRoute.html")
    },1000)
  }


/////////////////
  let shipDB = JSON.parse(localStorage.getItem('ShipList'));
  let shipList = shipDB ? shipDB : []
  async function getShips (){
    // document.getElementById('loader').style.display = "unset"
    const data = await  fetch('https://eng1003.monash/api/v1/ships/')
    .then(response => response.json())
    .then(data =>{
      return data
    });
    return data
  }
  getShips()
  .then(ship =>{
    console.log(ship)
    let allships = [...ship.ships, ...shipList]
    var jsonDeck = JSON.stringify(allships);
    var jsonDeck2 = JSON.stringify(shipList);
    console.log("hureeeeeeeeee")
    localStorage.setItem('ShipList', jsonDeck);

  })

  //Save ports
  let portDB = JSON.parse(localStorage.getItem('PortList'));
  let portList = portDB ? portDB : []
  async function AddPorts (){
    const data = await  fetch('https://eng1003.monash/api/v1/ports/')
    .then(response => response.json())
    .then(data =>{
      return data
    });
    return data
  }
  AddPorts()
  .then(port =>{
 
    let allPorts = [...port.ports, ...portList]
    var jsonDeck = JSON.stringify(allPorts);
    var jsonDeck2 = JSON.stringify(portList);
    console.log('Huraaaaaaaaaaah')
    localStorage.setItem('PortList', jsonDeck);
    // console.log(localStorage.getItem('PortList'))
    document.getElementById('loader').style.display = "none"
  })