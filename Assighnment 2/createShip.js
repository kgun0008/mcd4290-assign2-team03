<<<<<<< HEAD
<<<<<<< HEAD:Assighnment 2/js/createShip.js
        //Create shipsinfo
		let shipDB = JSON.parse(localStorage.getItem('ShipList'));
        let shipList = shipDB ? shipDB.shipList : [];
=======
        let shipDB = JSON.parse(localStorage.getItem('ShipList'));
=======


////////////Saving and retreiving shiplist


let shipDB = JSON.parse(localStorage.getItem('ShipList'));
>>>>>>> master
        let shipList = shipDB ? shipDB : [];
>>>>>>> master:Assighnment 2/createShip.js

        function addShip() {
            let ship = createShip();

            updateShipList(ship);
        }

        function createShip() {
            let shipSpecs = {
                name : document.getElementById('Name').value,
                speed : document.getElementById('Speed').value,
                range : document.getElementById('Range').value,
                description : document.getElementById('Description').value,
                cost : document.getElementById('Shipcost').value,
                status : document.getElementById('Status').value
            }

            return new Ship(shipSpecs);
        }

        function updateShipList(ship) {
            shipList.push(ship);

            if (typeof (Storage) !== "undefined") {

                var jsonDeck = JSON.stringify(shipList);


                localStorage.setItem('ShipList', jsonDeck);

                setTimeout(()=>{
                    window.location.replace("viewShips.html");
                  },1000)

            } else {
                console.log("Error: localStorage is not supported by current browser.");
            }
        }
//////////////////////////////clearing form

        function clearForm() {
            document.getElementById('Name').value = '';
            document.getElementById('Speed').value = '';
            document.getElementById('Range').value = '';
            document.getElementById('Description').value = '';
            document.getElementById('Shipcost').value = '';
            document.getElementById('Status').checked = true;
        }


//Intended to run initially
let shipDB = JSON.parse(localStorage.getItem('ShipList'));
let shipList = shipDB.shipList ? shipDB.shipList : []
async function getShips (){
  const data = await  fetch('https://eng1003.monash/api/v1/ships/')
  .then(response => response.json())
  .then(data =>{
    return data
  });
  return data
}
getShips()
.then(ship =>{
  let allships = [...ship.ships, ...shipList]
  var jsonDeck = JSON.stringify(allships);
  localStorage.setItem('ShipList', jsonDeck);
})

//Save ports
let portDB = JSON.parse(localStorage.getItem('PortList'));
let portList = portDB.portList ? portDB.portList : []
async function AddPorts (){
  const data = await  fetch('https://eng1003.monash/api/v1/ports/')
  .then(response => response.json())
  .then(data =>{
    return data
  });
  return data
}
AddPorts()
.then(port =>{
  let allPorts = [...port.ports, ...portList]
  var jsonDeck = JSON.stringify(allPorts);
  localStorage.setItem('PortList', jsonDeck);
})
