<<<<<<< HEAD
// creating view ports
=======

/////////////saving and retreving ports
>>>>>>> master
let portDB = JSON.parse(localStorage.getItem('PortList'));
  let portList = portDB ? portDB : [];
  console.log(portList)


  async function getPorts (){
    const data = await  fetch('https://eng1003.monash/api/v1/ports/')
    .then(response => response.json())
    .then(data =>{
       return data
    });
    return data
  }
  getPorts()
  .then(port =>{
    let allPorts = [...port.ports, ...portList]
    allPorts.forEach(port => {
      var portTR = createPortTR(port);
      insertPortTR(portTR);
    });
  })
/////////viewing ports
  function createPortTR(port){
    var {name, country, type, size, lat, lng} = port;
    let portRow = document.createElement("tr");
    let portRowContents = `
      <td>${name}</td>
      <td>${country}</td>
      <td>${type}</td>
      <td>${size}</td>
      <td>${lat}</td>
      <td>${lng}</td>
    `;
    portRow.innerHTML = portRowContents;

    return portRow;
  }

  function insertPortTR(portRow){
    document
      .querySelector("#portTableData")
      .appendChild(portRow);
  }