 //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      ////clear button

function clearForm(){
				document.getElementById('portName').value = '';
				document.getElementById('portCountry').value = '';
				document.getElementById('type').value = ''; 
				document.getElementById('portSize').value = '';
				document.getElementById('Latitude').value = '';
				document.getElementById('Longitude').value = '';
				document.getElementById('useAddress').checked = false;
			}
let portDB = JSON.parse(localStorage.getItem('PortList'));
let portList = portDB ? portDB : [];
console.log(portDB)
  portList.forEach(port => {
    var portTR = createPortTR(port);
    insertPortTR(portTR);
  });
//////////////////////fetching from the class
  function createPortTR(port){
    var {portname, country, type, size, latitude, longitude} = port;
    let portRow = document.createElement("tr");
    let portRowContents = `
      <td>${portname}</td>
      <td>${country}</td>
      <td>${type}</td>
      <td>${size}</td>
      <td>${latitude}</td>
      <td>${longitude}</td>
    `;
    portRow.innerHTML = portRowContents;

    return portRow;
  }

  function insertPortTR(portRow){
    document
      .querySelector("#portTableData")
      .appendChild(portRow);
  }

  // let portDB = JSON.parse(localStorage.getItem('PortList'));
  // let portList = portDB ? portDB.portList : [];
  // console.log(portList)
//////////////////////////creatiing port classs
  function addport() {
      let port = createPort();

      updatePortList(port);
      setTimeout(()=>{
        window.location.replace("viewPorts.html");
      },1000)
  }

  function createPort() {
      let portSpecs = {
          name : document.getElementById('portName').value,
          country : document.getElementById('portCountry').value,
          type : document.getElementById('type').value,
          size : document.getElementById('portSize').value,
          lat : document.getElementById('Latitude').value,
          lng : document.getElementById('Longitude').value
      }

      return new Port(portSpecs);
  }



//////////////////Updating port coordinate automatically
  function updatePortCordinate (){
      let portName = document.getElementById('portName').value;
      opencage
          .geocode({ q: portName, key: 'a7516151dbe14bcc8e052863e39ff59a' })
          .then(data => {
              console.log(data.results[0].geometry.lat);
              document.getElementById('Latitude').value = data.results[0].geometry.lat
              document.getElementById('Longitude').value = data.results[0].geometry.lng
          })
          .catch(error => {
              console.log('error', error.message);
          });
  }
  function updatePortList(port) {
      portList.push(port);
      let portDB2 = portList

      if (typeof (Storage) !== "undefined") {
          // TODO: Stringify deckInstance to a JSON string
          //var jsonDeck = JSON.stringify(port);
          var jsonPort = JSON.stringify(portDB2);

          // TODO: store this JSON string to local storage
          //       using the key STORAGE_KEY.
          //localStorage.setItem('ShipList', jsonDeck);
          localStorage.setItem('PortList', jsonPort);

          console.log(localStorage.getItem('PortList'));
      } else {
          console.log("Error: localStorage is not supported by current browser.");
      }
  }
  

