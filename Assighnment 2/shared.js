///////////////////creating class for ship
class Ship {
    constructor(shipSpecs) {
        var {name, range, speed, description, cost, status} = shipSpecs;
        this.name = name;
        this.range = range;
        this.speed = speed;
        this.description = description;
        this.cost = cost;
        this.status = status;
    }
}
////////////////creating class for port
class Port {
    constructor(portSpecs) {
        var {name, country, type, size, lat, lng} = portSpecs;
        this.name = name;
        this.country = country;
        this.type = type;
        this.size = size;
        this.lat = lat;
        this.lng = lng;
    }
}


//////////////creating class for route
class Route {
    constructor(routeSpecs) {
        var {ship,name, sourcePort, destinationPort, distance, time, cost, startDate, wayPointList} = routeSpecs
        this.ship = ship;
        this.startDate = startDate;
        this.sourcePort = sourcePort;
        this.cost = cost;
        this.destinationPort = destinationPort;
        this.distance = distance;
        this.time = time;
        this.name = name;
        // this.wayPointList = wayPointList
    }
}


class ShipList {
    contructor() {

    }
}



class PortList {
    contructor() {

    }
}




class RouteList {
    contructor() {

    }
}